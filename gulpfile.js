const gulp = require('gulp');
const sass = require('gulp-sass');
const browserSync = require("browser-sync").create();
const autoprefixer = require("gulp-autoprefixer");

gulp.task('sass', function () {
  return gulp.src('app/scss/main.scss')
    .pipe(sass().on("error", sass.logError))
    .pipe(sass())
    .pipe(autoprefixer({
      browsers: ['last 2 versions']
    }))
    .pipe(gulp.dest('app/css'))
    .pipe(browserSync.reload({
      stream: true
    }))
});

gulp.task('watch', ['browserSync', 'sass'], function() {
  gulp.watch('app/scss/**/*.scss', ['sass']);
  gulp.watch('app/*.html', browserSync.reload);
  gulp.watch('app/js/**/*.js', browserSync.reload); 
});

gulp.task('browserSync', function () {
  browserSync.init({
    server: {
      baseDir: 'app'
    },
  })
})