function shrinkMenuOnScroll() {
  
  shrinkMenuBars();
  // shrinkMenuLinks();
    function shrinkMenuBars() {
      let menuBars = document.getElementsByClassName("menu-bars");
      for (let i = 0; i < menuBars.length; i++) {
        if (document.body.scrollTop > 50 || document.documentElement.scrollTop > 50) {
          menuBars[i].style.padding = "0.5em 0";
        } else {
          menuBars[i].style.padding = "1.5em 0";
        }
      }
    }
    function shrinkMenuLinks() {
      let menuLinks = document.getElementsByClassName("menu-link");
      for (let i = 0; i < menuLinks.length; i++) {
        if (document.body.scrollTop > 50 || document.documentElement.scrollTop > 50
        ) {
          menuLinks[i].style.fontSize = "0.7em";
        } else {
          menuLinks[i].style.fontSize = "1em";
        }
      }
    }
}



window.onscroll = function() {
  shrinkMenuOnScroll();
};

function menuToggler() {
  let hamburger = document.getElementById('hamburger');
  let navToggler = document.querySelectorAll('.nav-toggler');
  
  for(let toggled of navToggler) {
    toggled.classList.toggle('show-hide-menu');
  }


}

$(".hamburger").click(function () {
  $(".down").toggleClass('downstart');
  $(".fade").toggleClass("fadestart");
  $(".up").toggleClass("upstart");
});
